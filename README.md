# MuseScore 4.1.1 no-sound patch

Due wrong source linux package missing in the instaled MuseScore 4.1 folder

`/usr/share/mscore-4.1/sound/`

where are located the sound fonts. This patch solving the problem.

Bun not solving the troubles with pipewire, so if you are using pipewire, then
you need switch to the pulseaudio.

Arch Linux users can for switch from pipewire to pulseaudio use this
[script](https://github.com/erikdubois/arcolinux-nemesis/blob/master/AUR/install-pulseaudio.sh).


## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/musescore4-no-sound-patch/):
```
git clone https://aur.archlinux.org/musescore4-no-sound-patch.git
cd musescore4-no-sound-patch/
makepkg -sci
```

### Install from source:
Unpack the source package and run command (like root):
```
./configure
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U distrib/xfce4-theme-switcher*.pkg.tar.xz
```
